/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.database.sevice;

import com.mycompany.database.dao.UserDao;
import com.mycompany.database.newpackage.user;

/**
 *
 * @author bbnpo
 */
public class Sevice {
    public user login(String name,String password){
        UserDao userDao = new UserDao();
        user user =userDao.getByName(name);
        if(user !=null && user.getPassword().equals(password)){
            return user;
        }
        return null;
    }
    
}
