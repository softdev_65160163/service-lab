/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.database.dao;

import com.mycompany.database.SelectDatabase;
import com.mycompany.database.helper.Helper;
import com.mycompany.database.newpackage.user;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bbnpo
 */
public class UserDao implements dao<user> {

    @Override
    public user get(int id) {
        user user = null;
        String sql = "SELECT * FROM user WHERE user_id=?";
        Connection conn = Helper.getConnection();
        try {
            PreparedStatement stat = conn.prepareStatement(sql);
            stat.setInt(1, id);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                user = new user();
                user.setId(rs.getInt("user_Id"));
                user.setName(rs.getString("user_name"));
                user.setRole(rs.getInt("user_role"));
                user.setGender(rs.getString("user_gender"));
                user.setPassword(rs.getString("user_password"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(SelectDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    @Override
    public List<user> getAll() {
        ArrayList<user> list = new ArrayList();

        String sql = "SELECT * FROM user";
        Connection conn = Helper.getConnection();
        try {
            Statement stat = conn.createStatement();
            ResultSet rs = stat.executeQuery(sql);
            while (rs.next()) {
                user user = new user();
                user.setId(rs.getInt("user_Id"));
                user.setName(rs.getString("user_name"));
                user.setRole(rs.getInt("user_role"));
                user.setGender(rs.getString("user_gender"));
                user.setPassword(rs.getString("user_password"));

                list.add(user);
            }

        } catch (SQLException ex) {
            Logger.getLogger(SelectDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }

    @Override
    public user save(user obj) {

        String sql = "INSERT INTO user (  user_name,  user_gender,user_password,user_role)" + " VALUES (?,?,?,?)";
        Connection conn = Helper.getConnection();
        try {
            PreparedStatement stat = conn.prepareStatement(sql);
            stat.setString(1, obj.getName());
            stat.setString(2, obj.getGender());
            stat.setString(3, obj.getPassword());
            stat.setInt(4, obj.getRole());
            System.out.println(stat);
            stat.executeUpdate();

            int id = Helper.getInsertID(stat);
            obj.setId(id);

        } catch (SQLException ex) {
            Logger.getLogger(SelectDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    @Override
    public user update(user obj) {
        String sql = "UPDATE user"
                + "   SET user_name = ?, user_gender = ?,user_password = ?,user_role = ?"
                + " WHERE user_id = ?";
        Connection conn = Helper.getConnection();
        try {
            PreparedStatement stat = conn.prepareStatement(sql);
            stat.setString(1, obj.getName());
            stat.setString(2, obj.getGender());
            stat.setString(3, obj.getPassword());
            stat.setInt(4, obj.getRole());
            stat.setInt(5, obj.getId());
            //System.out.println(stat);
            int ret = stat.executeUpdate();
            System.out.println(ret);
            return obj;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public int delete(user obj) {

        String sql = "DELETE  FROM user WHERE user_id=?";
        Connection conn = Helper.getConnection();
        try {
            PreparedStatement stat = conn.prepareStatement(sql);
            stat.setInt(1, obj.getId());
            int ret = stat.executeUpdate();
            return ret;

        } catch (SQLException ex) {
            Logger.getLogger(SelectDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public List<user> getAllOrderby(String name, String order) {
        ArrayList<user> list = new ArrayList();

        String sql = "SELECT * FROM user ORDER BY " + name + " " + order; // แก้คำสั่ง SQL ในนี้
        System.out.println("sql");
        Connection conn = Helper.getConnection();
        try {
            Statement stat = conn.createStatement();
            ResultSet rs = stat.executeQuery(sql);
            while (rs.next()) {
                user user = new user();
                user.setId(rs.getInt("user_Id"));
                user.setName(rs.getString("user_name"));
                user.setRole(rs.getInt("user_role"));
                user.setGender(rs.getString("user_gender"));
                user.setPassword(rs.getString("user_password"));

                list.add(user);
            }

        } catch (SQLException ex) {
            Logger.getLogger(SelectDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
  
    public user getByName(String name) {
        user user = null;
        String sql = "SELECT * FROM user WHERE user_name=?";
        Connection conn = Helper.getConnection();
        try {
            PreparedStatement stat = conn.prepareStatement(sql);
            stat.setString(1, name);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                user = new user();
                user.setId(rs.getInt("user_Id"));
                user.setName(rs.getString("user_name"));
                user.setRole(rs.getInt("user_role"));
                user.setGender(rs.getString("user_gender"));
                user.setPassword(rs.getString("user_password"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(SelectDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    @Override
    public List<user> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

   

}
